<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Permet d'appeler l'ensemble des lectures en une seule requête.
 * Ne nécessite aucun argument.
 *
 * @return void
 * @throws JsonException
 */
function action_api_evangile_dist() {
	// Initilisation du tableau des lectures
	$lectures = [];

	// Détermination des la date et de la langue souhaitées
	$arg = _request('arg');
	$arg = explode('/', $arg);
	$lang = array_shift($arg);
	$date = array_shift($arg);
	$date = strtotime($date);

	if (
		$lang
		and $date
		and !_IS_BOT
	) {
		// formater correctement la date
		$date = date('Y-m-d', $date);
		include_spip('services/evangelizo');
		$lectures = evangelizo_charger($lang, $date);
	}

	if (!function_exists('json_encode')) {
		include_spip('inc/json');
	}

	include_spip('inc/actions');
	ajax_retour(json_encode($lectures), 'text/json');
}
