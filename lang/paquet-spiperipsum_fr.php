<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'spiperipsum_description' => 'Ce plugin permet d\'afficher les différentes lectures de l\'évangile ou le saint du jour ou d\'un jour donné selon le calendrier fixé par l\'Eglise catholique. Ce service est proposé par le site de « [L\'Evangile au Quotidien->http://www.levangileauquotidien.org] » dans plusieurs langues.

L\'affichage des lectures se fait, d\'une part, via l\'utilisation d\'un modèle {{spiperipsum}} dans les squelettes utilisateur ou dans les articles, et d\'autre part,
via une noisette nommée {{spiperipsum}}.

Essayez la page de démo {demo/spiperipsum.html} pour des exemples d\'utilisation.',
	'spiperipsum_slogan' => 'Saint et évangile au quotidien',
);
