<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Langue par defaut si non supportee par le site serveur
if (!defined('_SPIPERIPSUM_LANGUE_DEFAUT')) {
	define('_SPIPERIPSUM_LANGUE_DEFAUT', 'en'); // services/evangelizo
}

// Jour par defaut
if (!defined('_SPIPERIPSUM_JOUR_DEFAUT')) {
	define('_SPIPERIPSUM_JOUR_DEFAUT', 'aujourdhui'); // fonctions, charger
}

// Valeurs de l'argument lecture dans le modele spiperipum
if (!defined('_SPIPERIPSUM_LECTURE_EVANGILE')) {
	define('_SPIPERIPSUM_LECTURE_EVANGILE', 'evangile');// services/evangelizo
}
if (!defined('_SPIPERIPSUM_LECTURE_PREMIERE')) {
	define('_SPIPERIPSUM_LECTURE_PREMIERE', 'premiere');// services/evangelizo
}
if (!defined('_SPIPERIPSUM_LECTURE_SECONDE')) {
	define('_SPIPERIPSUM_LECTURE_SECONDE', 'seconde');// services/evangelizo
}
if (!defined('_SPIPERIPSUM_LECTURE_PSAUME')) {
	define('_SPIPERIPSUM_LECTURE_PSAUME', 'psaume');// services/evangelizo
}
if (!defined('_SPIPERIPSUM_LECTURE_COMMENTAIRE')) {
	define('_SPIPERIPSUM_LECTURE_COMMENTAIRE', 'commentaire'); // fonctions
}
if (!defined('_SPIPERIPSUM_LECTURE_SAINT')) {
	define('_SPIPERIPSUM_LECTURE_SAINT', 'saint'); // fonctions
}
if (!defined('_SPIPERIPSUM_LECTURE_FETE')) {
	define('_SPIPERIPSUM_LECTURE_FETE', 'fete'); // inutilisé
}
if (!defined('_SPIPERIPSUM_LECTURE_DATE_TITRE')) {
	define('_SPIPERIPSUM_LECTURE_DATE_TITRE', 'date_titre'); // fonctions
}
if (!defined('_SPIPERIPSUM_LECTURE_DATE_ISO')) {
	define('_SPIPERIPSUM_LECTURE_DATE_ISO', 'date_iso'); // fonctions, date.html
}
if (!defined('_SPIPERIPSUM_LECTURE_DATE_LITURGIQUE')) {
	define('_SPIPERIPSUM_LECTURE_DATE_LITURGIQUE', 'date_liturgique'); // fonctions
}
// -- Lecture par defaut
if (!defined('_SPIPERIPSUM_LECTURE_DEFAUT')) {
	define('_SPIPERIPSUM_LECTURE_DEFAUT', 'evangile'); // fonctions
}
// -- Liste des lectures
if (!defined('_SPIPERIPSUM_LECTURE_LISTE')) {
	define(
		'_SPIPERIPSUM_LECTURE_LISTE',
		_SPIPERIPSUM_LECTURE_EVANGILE . ':' .
		_SPIPERIPSUM_LECTURE_PREMIERE . ':' .
		_SPIPERIPSUM_LECTURE_SECONDE . ':' .
		_SPIPERIPSUM_LECTURE_PSAUME . ':' .
		_SPIPERIPSUM_LECTURE_COMMENTAIRE . ':' .
		_SPIPERIPSUM_LECTURE_SAINT . ':' .
//		_SPIPERIPSUM_LECTURE_FETE . ':' .
		_SPIPERIPSUM_LECTURE_DATE_TITRE . ':' .
		_SPIPERIPSUM_LECTURE_DATE_ISO . ':' .
		_SPIPERIPSUM_LECTURE_DATE_LITURGIQUE
	); // fonctions
}

// Valeurs de l'argument mode d'appel du modele (depuis article ou page zpip)
if (!defined('_SPIPERIPSUM_MODE_ARTICLE')) {
	define('_SPIPERIPSUM_MODE_ARTICLE', 'article'); // date, lecture,saint, commentaire
}
if (!defined('_SPIPERIPSUM_MODE_PAGE')) {
	define('_SPIPERIPSUM_MODE_PAGE', 'page'); // date, lecture,saint, commentaire
}
// -- Mode par defaut
if (!defined('_SPIPERIPSUM_MODE_DEFAUT')) {
	define('_SPIPERIPSUM_MODE_DEFAUT', 'article');
}

// Info par defaut
if (!defined('_SPIPERIPSUM_INFO_DEFAUT')) {
	define('_SPIPERIPSUM_INFO_DEFAUT', 'titre'); // fonctions
}

// Séparateur entre la date iso et la date liturgique quand on demande une lecture=date
if (!defined('_SPIPERIPSUM_SEPARATEUR_DATE')) {
	define('_SPIPERIPSUM_SEPARATEUR_DATE', ',&nbsp;'); // services/envangelizo
}
