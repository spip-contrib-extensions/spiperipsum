<?php
/**
 * Ce fichier contient les fonctions de service nécessitées par le plugin Cache Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des types de cache de SpiperIpsum.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array<string, mixed> Tableau de la configuration des types de cache du plugin.
 */
function spiperipsum_cache_configurer($plugin) {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	return [
		'lecture' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => true,
			'nom_obligatoire' => ['date', 'langage'],
			'nom_facultatif'  => [],
			'extension'       => '.txt',
			'securisation'    => false,
			'serialisation'   => true,
			'separateur'      => '_',
			'conservation'    => 86400 * 365
		],
	];
}
